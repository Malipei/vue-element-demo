import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'

import nav1 from '@/views/nav1/nav1.vue'
import nav2 from '@/views/nav1/nav2.vue'
import err from '@/components/404.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      redirect: '/nav1-1',
      component: Home,
      children: [{
        path: '/nav1-1',
        name: 'nav1',
        component: nav1
      },
      {
        path: '/nav1-2',
        name: 'nav2',
        component: nav2
      }
      ]
    },
    {
      path: '/404',
      name: '404',
      component: err
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})
